#!/usr/bin/python3

import socket
import sys
import getopt
import time
from gmpy2 import *
from Crypto.Random import random
from Crypto.Cipher import AES

b = mpz(68366528803802774494102028092185614536187281887082630883946649435775005432542)

def main(argv):
    host = ''
    port = -1
    try:
        opts, args = getopt.getopt(argv, "h:p:")
    except getopt.GetoptError:
        print('server.py -h -p')

    for opt, arg in opts:
        if opt == '-h':
            host = arg
            print(host)
        elif opt == '-p':
            port = int(arg)
            print(port)

    s = socket.socket()
    s.bind((host, port))
    s.listen(5)
    while True:
        c, addr = s.accept()
        print('Connection from', addr)
        # Receive and parse parameters.
        c.send('STARTING KEY EXCHANGE PROTOCOL'.encode())
        param = c.recv(2048)
        params = param.split("|".encode())
        g = mpz(params[0].split("g: ".encode())[1])
        p = mpz(params[1].split("mod: ".encode())[1])
        clientPublic = mpz(params[2].split("pub: ".encode())[1])

        print("Generator: " + str(g))
        print("Modulus: " + str(p))
        print("Public Key: " + str(clientPublic))

        # Do our computation.
        # g^b mod p
        serverPublic = pow(g, b, p)
        secret = pow(clientPublic, b, p)
        print("SECRET", secret)
        aes_key = random.getrandbits(128)
        print("AES KEY", aes_key)
        cipher_key = mul(aes_key, secret) % p

        c.send("pub: ".encode() + serverPublic.digits(10).encode() + "| enc_key: ".encode() + cipher_key.digits(10).encode())

        enc_message = c.recv(4096)
        cipher = AES.new(int(aes_key).to_bytes(16, byteorder='big'), AES.MODE_ECB)
        #cipher = AES.new(to_binary(aes_key), AES.MODE_ECB)
        pt = cipher.decrypt(enc_message)
        print("FLAG:", pt)

        time.sleep(5)

        c.close()

if __name__ == "__main__":
    main(sys.argv[1:])
