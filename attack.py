from gmpy2 import *
import random as rand

def factorize(n):
    temp = n 
    i = 2
    result = [] 
    while i * i <= n:
        if n % i:
            i += 1
        else:
            exp = 0
            while not (n % i):
                n //= i
                exp += 1
            result += [(i, exp)]
    if n is not 1:
        result += [(temp, 1)]
    return result

def modInv(x, n):
    def xgcd(b, a):
        x0, x1, y0, y1 = 1, 0, 0, 1
        while a != 0:
            q, b, a = b // a, a, b % a
            #q, b, a = divexact(mpz(b), mpz(a)), a, b % a
            x0, x1 = x1, x0 - q * x1
            y0, y1 = y1, y0 - q * y1
        return x0     

    return xgcd(x, n) % n

def findSubExponents(factors, modulus, generator, target):
    return [findSubExponent(factor, modulus, generator, target) for factor in factors]

def findSubExponent(factor, modulus, generator, target):
    #print('Modulus: {}'.format(modulus))
    #print(factor, target)

    base, exponent = factor[0], factor[1]
    cfs = [base**(i+1) for i in range(0, exponent)]
    #print("Coefficients: {}".format(cfs))
    result = 0
    
    m_static = divexact(mpz(modulus-1), mpz(cfs[0]))
    #m_static = int((modulus-1)/cfs[0])

    for cf in cfs:
       #print('Target: {}'.format(target))
       m = divexact(mpz(modulus-1), mpz(cf)) 
       #print("Current exponent: {}".format(m))
       G = powmod(mpz(target), m, mpz(modulus))
       H = powmod(mpz(generator), m_static, mpz(modulus))

       val = None
       for i in range(0, base):
           p = powmod(mpz(H), mpz(i), mpz(modulus))
           #print(G, p)
           if p == G:
               val = i
               break
       
       if val is None:
           print("There was a critical error.")
           #val = 100
           #if i cheat here, things seem to work
           #return exp 
           return
       else: 
           result += val * int(cf/base)

       inv_pow = int(cf/base) * val
       inv = modInv(pow(generator, inv_pow, modulus), modulus)
       #print("Inverse: {}".format(inv))
       target = (target * inv) % modulus

    return result
         
def crt(nums, factors, modulus):
      factors = [pow(factor[0], factor[1], modulus) for factor in factors]
      #M_arr = [int((modulus-1)/factor) for factor in factors]
      M_arr = [divexact(mpz(modulus-1), mpz(factor)) for factor in factors]
      y_arr = [modInv(M, factor) for M, factor in zip(M_arr, factors)]
      result = sum([num*M*y for num, M, y in zip(nums, M_arr, y_arr)]) % (modulus-1)
      return result

def findgen(p):
    o = p-1
    factors = factorize(o)
    for g in range(2,p):
        if all(pow(g, o//fp, p) is not 1 for fp, _ in factors):
            return g

modulus = 667247790729629168801868651264033309682519458519152541737787866083418205742829005514311734375000001
#modulus = 113578739029156612876450129204242905998910294171436254833388853651455710893740275170987183521536054548030282117195498457692489893406823886862738752684652079978936733074232273368577525967037159987046815343234039167216297756514663558282660842724884844179677825315893474089066334390409011608169928022879668851645217566449125000000000000001
#modulus = 52501
#modulus = 8101
#any number i pick here seems to work... idk
generator = findgen(modulus)
print(generator)
#to be discovered
#exp = 100000000000000
#exp = rand.randint(2 ** 255, 2 ** 256)
exp = 68366528803802774494102028092185614536187281887082630883946649435775005432542
print(exp)

target = pow(generator, exp, modulus)
factors = factorize(modulus-1)
print("Modulus factors: ", factors)
nums = findSubExponents(factors, modulus, generator, target)
print("Nums: ", nums)
print(crt(nums, factors, modulus))

#while(1):
#    try:
#        print(exp)
#        target = pow(generator, exp, modulus)
        #target = 7531

#        factors = factorize(modulus-1)
#        print("Modulus factors: ", factors)
#        nums = findSubExponents(factors, modulus, generator, target)
#        print("Nums: ", nums)
#        print(exp, crt(nums, factors, modulus))
#        break
#    except:
#        exp += 2 * 23
