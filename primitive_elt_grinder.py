#from operations import *
from gmpy2 import *

#modulus = mpz(113578739029156612876450129204242905998910294171436254833388853651455710893740275170987183521536054548030282117195498457692489893406823886862738752684652079978936733074232273368577525967037159987046815343234039167216297756514663558282660842724884844179677825315893474089066334390409011608169928022879668851645217566449125000000000000001)

#factors = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67]

#medium sized mod
#modulus = mpz(667247790729629168801868651264033309682519458519152541737787866083418205742829005514311734375000001)
#factors = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
#factors = [mpz(factor) for factor in factors]

modulus = 1470
factors = [2, 3, 5, 7]


exponents = [mpz((modulus-1)/factor) for factor in factors]
print(exponents)

val = 0

for num in range(int(2), modulus):
    i = mpz(num)
    check = True

    for exponent in exponents:
        print("Exponent is: {}".format(exponent))
        test = pow(i, exponent, modulus)
        print("Power is: {}".format(test))
        if test == 1:
            print(test)
            check = False
    if check:
        val = i
        break

print("The value is: {}".format(val))
print(pow(val, int((modulus-1)/2), modulus))
