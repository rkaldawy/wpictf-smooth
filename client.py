#!/usr/bin/python3

import socket
import sys
import getopt
import time
from gmpy2 import *
from Crypto.Cipher import AES

g = mpz(10)
a = mpz(2100)
p = mpz(667247790729629168801868651264033309682519458519152541737787866083418205742829005514311734375000001)

flag = b'WPI{sTRuk_byA_$m0otH_cR!mIn@1}'

def pad(s):
    s += b''.join([bytes(chr(0), 'utf-8') for i in range(0, (16 - (len(s) % 16)))])
    return s

def modInv(x, n):
    def xgcd(b, a):
        x0, x1, y0, y1 = 1, 0, 0, 1
        while a != 0:
            q, b, a = b // a, a, b % a
            #q, b, a = divexact(mpz(b), mpz(a)), a, b % a
            x0, x1 = x1, x0 - q * x1
            y0, y1 = y1, y0 - q * y1
        return x0     

    return xgcd(x, n) % n

def main(argv):
    host = ''
    port = -1
    try:
        opts, args = getopt.getopt(argv, "h:p:")
    except getopt.GetoptError:
        print('server.py -h -p')

    for opt, arg in opts:
        if opt == '-h':
            host = arg
            print(host)
        elif opt == '-p':
            port = int(arg)
            print(port)
    s = socket.socket()

    s.connect((host, port))
    print(s.recv(1024))

    # Compute and send our computation.
    # g^a mod p
    clientPublic = pow(g, a, p)
    print(clientPublic)

    s.send('g: '.encode() + g.digits(10).encode() + "| mod: ".encode() + p.digits(10).encode() + "| pub: ".encode() + clientPublic.digits(10).encode())

    # Receive the server's computation
    param = s.recv(4096)
    params = param.split("|".encode())

    serverPublic = mpz(params[0].split("pub: ".encode())[1])
    cipher_key = mpz(params[1].split("enc_key: ".encode())[1])
    
    # Compute the shared secret.
    secret = pow(serverPublic, a, p)
    aes_key = mul(modInv(secret, p), cipher_key) % p 
    print("SECRET", secret)
    print("AES KEY", aes_key)

    print(len(int(aes_key).to_bytes(16, byteorder='big')))
    cipher = AES.new(int(aes_key).to_bytes(16, byteorder='big'), AES.MODE_ECB)
    ct = cipher.encrypt(pad(flag))
    print("ENCRYPTED FLAG", ct)
    s.send(ct)
    
    time.sleep(4)
    s.send(b'v=h_D3VFfhvs4')

    s.close()

if __name__ == "__main__":
    main(sys.argv[1:])
